# choco-constraints-java

Nantes, FRANCE java constraints programming (first contact Google OR-Tools python https://developers.google.com/optimization/cp/ https://github.com/google/or-tools)

From http://www.choco-solver.org/

https://github.com/chocoteam/choco-solver

(alternative : http://www.lth.se/jacop/ https://github.com/radsz/jacop)

+++++ Global constraints

 satisfaction and optimization (mono and multi) problems, multi-thread resolution,

Common formulae http://www.csplib.org/Problems/prob047/

http://www.csplib.org/Problems/prob049/